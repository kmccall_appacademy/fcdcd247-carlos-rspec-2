require "time"

def measure(times_to_run = 1, &block)
  measurements = []
  times_to_run.times do
    starttime = Time.now
    block.call
    measurements << Time.now - starttime
  end
  measurements.inject(:+) / measurements.size
end
