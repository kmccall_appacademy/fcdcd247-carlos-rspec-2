def reverser(&block)
  words = block.call.split(" ")
  result = words[0].reverse
  words.drop(1).each do |word|
    result += " " + word.reverse
  end
  result
end

def adder(to_add = 1, &block)
  block.call + to_add
end

def repeater(times = 1, &block)
  times.times { block.call }
end
